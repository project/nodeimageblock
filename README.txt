Module: Node Image Block
Author: Mike Carter <mike@ixis.co.uk>


Description
===========
Displays all images that are attached to any node using the upload.module.


Requirements
============

* Drupal 4.5+ installation
* upload.module to attach images to nodes


Installation
============
* Copy the 'nodeimageblock' module directory in to your Drupal
modules directory as usual.


Usage
=====
When editing a node, attach an image to it using the upload.module functionality.

Untick the 'list' option to prevent the image(s) from being listed to
the public at the end of the node.

In the Blocks configuration page (?q=admin/block) enable the 'Node Image' block
and assign it to a region on the page.




Security
========
To restrict who can see your 'contact card' nodes I suggest using the Simple_Access module.
Available from http://drupal.org/project/simple_access

